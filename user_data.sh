PUBLIC_IP=$1
PRIVATE_IP=$2
if [[ $PUBLIC_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "initialising"
else
  echo "No valid IP found"
  exit 1

fi
ssh -o "StrictHostKeyChecking=no" -i ~/.ssh/cohort8ijazssh.pem ubuntu@$PUBLIC_IP ' 
sudo sh -c "cat >/etc/hosts << _END_
127.0.0.1 localhost

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
Cohort8-Ijaz-LB $LB_IP
sudo apt update


sudo apt install nginx -y

sudo systemctl status nginx

sudo ufw allow 'nginx full'
sudo sh -c "cat >/var/www/html/index.nginx-debian.html << _END_
<h1>
NGINX
</h1>
<p> Hello my name is IJaz and this is NGINX my private IP is</p>
_END_"
'